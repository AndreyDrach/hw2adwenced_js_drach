// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// Коли нам хотілося б при помилках якось контролювати ситуацію, щоб скрипт не просто впав, а зробив щось розумне.


const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

class NotInfoError extends Error {
  constructor() {
    super();
    this.name = "NotInfoError";
    this.message = "Not enough information";
  }
}

const div = document.getElementById("root");
div.innerHTML = "<ul> </ul>";
const ul = div.querySelector("ul");

class BooksCollection {
  constructor(book) {
    if (
      book.author === undefined ||
      book.price === undefined ||
      book.name === undefined
    ) {
      throw new NotInfoError();
          }
    this.author = book.author;
    this.name = book.name;
    this.price = book.price;
  }
  render(ul) {
    ul.insertAdjacentHTML(
      "beforeEnd",
      `<li> author: ${this.author}, name: ${this.name}, price: ${this.price}</li>`
    );
  }
}

books.forEach((el) => {
  try {
     new BooksCollection(el).render(ul);
  } catch (err) {
    if (err.name === "NotInfoError") {
      console.warn(err);
    }
  }
});
